import uuid


def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))
if __name__ == '__main__':
    print_random_uuids(2)


def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number+1):
        if number % 7 == 0:
            print(number)


def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
    temp_fahrenheit = temp * 9 / 5 + 32
    print(f'Celsius: {temp}, Fahrenheit: {temp_fahrenheit}')

if __name__ == '__main__':
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]