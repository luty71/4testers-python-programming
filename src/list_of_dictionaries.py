# animal = {
#     "kind": "dog",
#     "age": 2,
#     "male": True
# }
# animal2 = {
#     "kind": "cat",
#     "age": 5,
#     "male": True
# }
#
# animal_kinds = ["dog", "cat", "fish"]
#
# animals = [
#     {
#     "kind": "dog",
#     "age": 2,
#     "male": True
#     },
#     {
#     "kind": "cat",
#     "age": 5,
#     "male": True
#     },
#     {
#     "kind": "fish",
#     "age": 1,
#     "male": True
#     }
# ]
#
# print(len(animals))
# print(animals[0])
# print(animals[-1])
# last_animal = animals[-1]
# last_animal_age = last_animal["age"]
# print("The age of the last animal is  equal to", last_animal_age)
# print(animals[0]["male"])
# print(animals[1]["kind"])
#
# animals.append(
#     {
#         "kind": "zebra",
#         "age": 7,
#         "male": False
#     }
# )
# print(animals)

list_of_adress = ["adress1", "adress2", "adress3"]

adresses = [
    {
        "city": "Warszawa",
        "street": "Polna",
        "House_no": "2",
        "post_code": "02-388"
    },
    {
        "city": "Wrocław",
        "street": "Zielna",
        "House_no": "4",
        "post code": "50-348"
    },
    {
        "city": "Poznań",
        "street": "Kasztanowa",
        "House_no": "8",
        "post_code": "35-378"
    },
]

print(adresses[-1]["post_code"])
print(adresses[1]["city"])
adresses[0]["street"] = "Pasikurowice"
print(adresses)