first_name = "Adrian"
last_name = "Luty"
email = "luty71@gmail.com"

print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."
print(my_bio)

# F-strings

my_bio_using_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}.\nMój email to {email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 razy 5 to {4 * 5}")

### Algebra ###
circle_radius = 4
area_of_a_circle_with_radius_5 = 3.14 * 5 ** 2
circumference_of_a_circle_with_radius_5 = 2 * 3.14 * 5

print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle_with_radius_5)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius_5)

long_math_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7 ** 2 - 13
print(long_math_expression)


my_name = "Adrian"
favourite_movie = "Smoleńsk"
favourite_actor = "Tomasz Karolak"

bio = (f"Mam na imię {my_name}.\nMój ulubiony film to {favourite_movie}.\nMój ulubiony aktor to {favourite_actor}.")
print(bio)

def print_name_and_city(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście {city}!")

print_name_and_city("Michał", "Toruń")
print_name_and_city("Beata", "Gdynia")

def generate_email_adress(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"

print(generate_email_adress("Janusz", "Nowak"))
print(generate_email_adress("Barbara", "Kowalska"))