def get_country_of_a_car_brand(car_brand):
        if car_brand_lowercase in ('Toyota', 'Mazda', 'Suzuki', 'Subaru'):
            return 'Japan'
        elif car_brand_lowercase in ('BMW', 'Mercedes', 'Audi', 'Volkswagen'):
            return 'Germany'
        elif car_brand_lowercase in ('Renault', 'Peugeot'):
            return 'France'
        else:
            return 'Unknown'

def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand('Toyota') == 'Japan'

