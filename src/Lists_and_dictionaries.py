shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
print(shopping_list[0])
print(shopping_list[2])
print(shopping_list[-1])
print(shopping_list[-2])
shopping_list.append('lemons')
print(shopping_list)
print(shopping_list[-1])

numer_of_items_to_buy = len(shopping_list)
print(numer_of_items_to_buy)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}
dog_age = animal["age"]
print("dog age:", dog_age)
dog_name = animal["name"]
print("dog name:", dog_name)

animal["age"] = 10
print(animal)
animal["owner"] = "Janusz"
print(animal)

friend = {
    "name": "Paweł",
    "age": 35,
    "hobbies": ["football", "basketball"],
}
print(friend["age"])
friend["city"] = "Wrocław"
friend["age"] = 36
print(friend)
friend["job"] = "doctor"
print(friend)
del friend["job"]
print(friend)

def get_added_numbers(list_of_numbers)
    return sum(list_of_numbers)

temperatures_in_january = [-4, 1.0, -7, 2]
temperatures_in_february = [-13, -9, -3, 3]

print(f"")
